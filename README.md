# B6B36PJC - Semestralni prace

Kalkulačka matic - plus, mínus, spodní a dolní trojúhelník.
Semestralni prace vypracovala Margarita Gorbatenko.

Počitací funkce se nachází ve třídě 'matrix'. Testovací data se nachází v souborech cmake-build-debug/file1.txt (první matice) a cmake-build-debug/file2.txt (druhá matice), program má naimplementované několik přepínačů: 
--help
--m1+m2
--m1-m2
--m1+c
--m1-c
--upper_m1
--lower_m1
--end.


Popis programu:

The program calculates the sum of matrices, the difference of matrices, the sum of matrix and scalar, the difference of matrix and scalar, upper and lower triangular matrices. Program read matrices from files file1.txt and file2.txt (cmake-build-debug package). Attention! Note that first number in the text file represents number of rows in the matrix. Second number represents number of columns. Your matrix comes only after these two values.
Sum: matrix from file1.txt + matrix from file2.txt. 
Difference: matrix from file1.txt - matrix from file2.txt. 
To calculate sum choose command '--m1+m2'. To calculate difference choose command '--m1-m2'.
Also you can calculate sum/difference between matrix and constant value with commands '--m1+c' or '--m1-c'.
The program also calculates upper and lower triangular matrices from 'file1.txt' file. To calculate upper triangular matrix choose command '--upper_m1'. To calculate lower triangular matrix choose command '--lower_m1'. If you want to run program, rerun and choose one the options.

#include <vector>

using namespace std;

class matrix {
    vector<vector<double>> matrix_repr;

    int num_of_rows{};
    int num_of_columns{};

public:
    matrix(int, int);
    explicit matrix(const char *);
    matrix(const matrix& rhs) = default;
    ~matrix() = default;

    matrix operator+(matrix &);
    matrix operator+(int scalar);
    matrix operator-(matrix &);
    matrix operator-(int scalar);


    void print() const;
    void upper_triangular_matrix();
    void lower_triangular_matrix();

    int getRows() const;

    int getCols() const;
};
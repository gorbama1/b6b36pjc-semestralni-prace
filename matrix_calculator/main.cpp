#include <iostream>
#include <chrono>
#include "matrix.h"

using namespace std;


int main() {
    string input;
    cout << "Before start, write down number of rows and columns and your matrix to files file1.txt and file2.txt in cmake-build-debug package. "
            "\nPlease note that first number in the file represents number of rows in the matrix. "
            "Second number represents number of columns.\nYour matrix comes only after these two values."
            "\nCommands:\n--help\n--m1+m2\n--m1-m2\n--m1+c\n--m1-c\n--upper_m1\n--lower_m1\n--end\nWrite command: ";
    cin >> input;

    if(input == "--help")
        cout << "The program calculates the sum of matrices, the difference of matrices, the sum of matrix and scalar, the difference of matrix and scalar, "
                "upper and lower triangular matrices. Program read matrices from files file1.txt and file2.txt (cmake-build-debug package)."
                "Attention! Note that first number in the text file represents number of rows in the matrix. "
                "\nSecond number represents number of columns. Your matrix comes only after these two values."
                "\nCommands:"
                "\nSum: matrix from file1.txt + matrix from file2.txt;"
                "\nDifference: matrix from file1.txt - matrix from file2.txt;"
                "\nTo calculate sum choose command '--m1+m2'. To calculate difference choose command '--m1-m2'."
                "\n Also you can calculate sum/difference between matrix and constant value with commands '--m1+c' or '--m1-c'"
                "\nThe program also calculates upper and lower triangular matrices from 'm1.txt' file. "
                "\nTo calculate upper triangular matrix choose command '--upper_m1'."
                "\nTo calculate lower triangular matrix choose command '--lower_m1'."
                "\n\nIf you want to run program, rerun and choose one the options\n" << endl;

    else if(input == "--m1+m2"){
        auto start = std::chrono::high_resolution_clock::now();
        matrix m1 = matrix("file1.txt");
        matrix m2 = matrix("file2.txt");
        cout << "\nMatrix 1:" << endl;
        m1.print();
        cout << "\nMatrix 2:" << endl;
        m2.print();
        cout << "\nSum:" << endl;
        try {
            (m1 + m2).print();
        } catch(exception &e){
            cout << "The number of rows and columns in the first matrix must be equal to the number of rows and columns in the second matrix.";
        }

        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "\nSpent " <<  chrono::duration<double, std::milli>(end-start).count() << "ms.\n";
    }else if(input == "--m1-m2") {
        auto start = std::chrono::high_resolution_clock::now();
        matrix m1 = matrix("file1.txt");
        matrix m2 = matrix("file2.txt");
        cout << "\nMatrix 1:" << endl;
        m1.print();
        cout << "\nMatrix 2:" << endl;
        m2.print();
        cout << "\nDifference:" << endl;
        try {
            (m1 - m2).print();
        } catch(exception &e){
            cout << "The number of rows and columns in the first matrix must be equal to the number of rows and columns in the second matrix.";
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "\nSpent " <<  chrono::duration<double, std::milli>(end-start).count() << "ms.\n";
    } else if (input == "--m1+c") {
        auto start = std::chrono::high_resolution_clock::now();
        int in = 0;
        cout << "Write the scalar: ";
        cin >> in;
        matrix m1 = matrix("file1.txt");
        cout << "\nMatrix 1:" << endl;
        m1.print();
        cout << "\nSum:" << endl;
        try {
            (m1 + in).print();
        } catch(exception &e){
            cout << "Error";
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "\nSpent " <<  chrono::duration<double, std::milli>(end-start).count() << "ms.\n";
    }else if (input == "--m1-c") {
        auto start = std::chrono::high_resolution_clock::now();
        int in = 0;
        cout << "Write the scalar: ";
        cin >> in;
        matrix m1 = matrix("file1.txt");
        cout << "\nMatrix 1:" << endl;
        m1.print();
        cout << "\nSum:" << endl;
        try {
            (m1 - in).print();
        } catch(exception &e){
            cout << "Error";
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "\nSpent " <<  chrono::duration<double, std::milli>(end-start).count() << "ms.\n";
    }else if(input == "--upper_m1") {
        auto start = std::chrono::high_resolution_clock::now();
        matrix m1 = matrix("file1.txt");
        cout << "\nMatrix 1:" << endl;
        m1.print();
        cout << "\nUpper triangular matrix:" << endl;
        try {
            m1.upper_triangular_matrix();
            m1.print();
        } catch (exception &e) {
            cout << "The number of columns in the matrix must be equal to the number of rows.";
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "\nSpent " << chrono::duration<double, std::milli>(end - start).count() << "ms.\n";
    }else if(input == "--lower_m1") {
        auto start = std::chrono::high_resolution_clock::now();
        matrix m1 = matrix("file1.txt");
        cout << "\nMatrix 1:" << endl;
        m1.print();
        cout << "\nLower triangular matrix:" << endl;
        try {
            m1.lower_triangular_matrix();
            m1.print();
        } catch(exception &e){
            cout << "The number of columns in the matrix must be equal to the number of rows.";
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "\nSpent " <<  chrono::duration<double, std::milli>(end-start).count() << "ms.\n";
    } else if(input == "--end")
        return 0;
    else{
        cout << "Wrong command! Please rerun and choose one of the commands below:"
                "\nCommands:\n--help\n--m1+m2\n--m1-m2\n--m1+c\n--m1-c\n--upper_m1\n--lower_m1\n--end";
    }
}
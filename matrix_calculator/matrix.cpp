#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <bits/unique_ptr.h>
#include "matrix.h"

using namespace std;

matrix::matrix(int rowSize, int colSize){
    num_of_rows = rowSize;
    num_of_columns = colSize;
    matrix_repr.resize(rowSize);
    for (auto & i : matrix_repr){
        i.resize(colSize);
    }
}


matrix::matrix(const char * file_name) {
    ifstream file(file_name);
    string line;
    int row = 0;
    int col = 0;


    if (file.is_open()) {
        file >> row;
        file >> col;

        if ((row <= 0) || (col <= 0)) {
            throw invalid_argument("Invalid value");
        } else {
            matrix_repr.resize(row);
            for (auto & i : matrix_repr){
                i.resize(col);
            }

            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    file >> matrix_repr[i][j];
                }
            }
        }

        num_of_columns = col;
        num_of_rows = row;
        file.close();
    }

}


matrix matrix::operator+(matrix &matrix2){
    if(matrix2.getCols() == this->getRows() && matrix2.getCols() == this->getCols()) {
        matrix result(num_of_columns, num_of_rows);
        for (int i = 0; i < num_of_rows; i++) {
            for (int j = 0; j < num_of_columns; j++) {
                result.matrix_repr[i][j] = this->matrix_repr[i][j] + matrix2.matrix_repr[i][j];
            }
        }
        return result;
    }
    throw std::exception();
}



matrix matrix::operator-(matrix & matrix2){
    if(matrix2.getCols() == this->getRows() && matrix2.getCols() == this->getCols()) {
        matrix result(num_of_columns, num_of_rows);
        for (int i = 0; i < num_of_rows; i++) {
            for ( int j = 0; j < num_of_columns; j++) {
                result.matrix_repr[i][j] = this->matrix_repr[i][j] - matrix2.matrix_repr[i][j];
            }
        }

        return result;
    }
    throw std::exception();
}


void matrix::print() const{
    for (int i = 0; i < num_of_rows; i++) {
        for (int j = 0; j < num_of_columns; j++) {
            if(isnan(matrix_repr[i][j])) {
                cout << "[" << 0 << "] ";
            } else {
                cout << fixed << "[" << matrix_repr[i][j] << "] ";
            }
        }
        cout << endl;
    }
}

int matrix::getRows() const{
    return this->num_of_rows;
}


int matrix::getCols() const{
    return this->num_of_columns;
}


matrix matrix::operator+(int scalar){
    matrix result(num_of_rows,num_of_columns);
    for (int i = 0; i < num_of_rows; i++){
        for (int j = 0; j < num_of_columns; j++){
            result.matrix_repr[i][j] = this->matrix_repr[i][j] + scalar;
        }
    }
    return result;
}

matrix matrix::operator-(int scalar){
    matrix result(num_of_rows,num_of_columns);
    for (int i = 0; i < num_of_rows; i++)
    {
        for (int j = 0; j < num_of_columns; j++)
        {
            result.matrix_repr[i][j] = this->matrix_repr[i][j] - scalar;
        }
    }
    return result;
}


void matrix::upper_triangular_matrix(){

    if(this->getRows() == this->getCols()) {
        int size = this->num_of_rows;

        double temp;
        for (int k = 1; k < size; k++) {
            for (int j = k; j < size; j++) {
                temp = this->matrix_repr[j][k - 1] / this->matrix_repr[k - 1][k - 1];
                for (int i = 0; i < size; i++) {
                    this->matrix_repr[j][i] = this->matrix_repr[j][i] - temp * this->matrix_repr[k - 1][i];
                }
            }
        }
    }
    throw std::exception();

}


void matrix::lower_triangular_matrix() {
    if(this->getRows() == this->getCols()) {
        int size =  (int)num_of_rows;

        double temp;
        for (int k = size-1; k >= 0; k--) {
            for (int j = k; j > 0; j--) {
                temp = this->matrix_repr[j-1][k] / this->matrix_repr[k][k];
                for (int i = k; i >= 0; i--) {
                    this->matrix_repr[j-1][i] = this->matrix_repr[j-1][i] - temp * this->matrix_repr[k][i];
                }
            }
        }
    }
    throw std::exception();
}
